/*
 * Copyright (C) 2020 Team QUIPSY
 */

package de.quipsy.cli.spi;

@SuppressWarnings("serial")
public class CommandException extends Exception {

    public CommandException() {
        // intentionally left blank
    }

    public CommandException(final String message) {
        super(message);
    }

    public CommandException(final Throwable cause) {
        super(cause);
    }

    public CommandException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public CommandException(final int status) {
        this.status = status;
    }

    public CommandException(final String message, final int status) {
        super(message);
        this.status = status;
    }

    public CommandException(final Throwable cause, final int status) {
        super(cause);
        this.status = status;
    }

    public CommandException(final String message, final Throwable cause, final int status) {
        super(message, cause);
        this.status = status;
    }

    private int status = 1;

    public final int getStatus() {
        return this.status;
    }

}
