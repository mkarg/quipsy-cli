/*
 * Copyright (C) 2019 ProSeS BDE GmbH
 */

package de.quipsy.cli.jcommander;

import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.beust.jcommander.IDefaultProvider;

/**
 * A default provider that reads its default values from an environment variable.
 *
 * @author Markus KARG (karg@quipsy.de)
 */
public final class EnvironmentVariableDefaultProvider implements IDefaultProvider {

    private final String environmentVariableValue;

    private final String optionPrefixesPattern;

    /**
     * For Unit Tests Only: Allows to mock the resolver, as Java cannot set environment variables.
     *
     * @param environmentVariableName
     *            The name of the environment variable to read.
     * @param optionPrefixes
     *            A set of characters used to indicate the start of an option (e. g. "-/" if option names may start with either dash or slash).
     */
    public EnvironmentVariableDefaultProvider(final String environmentVariableName, final String optionPrefixes) {
        this(environmentVariableName, optionPrefixes, System::getenv);
    }

    /**
     * For Unit Tests Only: Allows to mock the resolver, as Java cannot set environment variables.
     *
     * @param environmentVariableName
     *            The name of the environment variable to read.
     * @param optionPrefix
     *            A set of characters used to indicate the start of an option (e. g. "-/" if option names may start with either dash or slash).
     * @param resolver
     *            Reads the value from the environment variable.
     */
    EnvironmentVariableDefaultProvider(final String environmentVariableName, final String optionPrefixes, final Function<String, String> resolver) {
        this.environmentVariableValue = resolver.apply(environmentVariableName);
        this.optionPrefixesPattern = Optional.ofNullable(optionPrefixes).map(Pattern::quote).orElse("");
    }

    @Override
    public final String getDefaultValueFor(final String optionName) {
        if (this.environmentVariableValue == null)
            return null;
        final Matcher matcher = Pattern
                .compile("(?:(?:.*\\s+)|(?:^))(" + Pattern.quote(optionName) + ")\\s*((?:[^" + this.optionPrefixesPattern + "\\s]+)|(?:'.*')|(?:\".*\"))?.*")
                .matcher(this.environmentVariableValue);
        if (!matcher.matches())
            return null;
        String value = matcher.group(2);
        if (value == null)
            return "true";
        final char firstCharacter = value.charAt(0);
        if (firstCharacter == '\'' || firstCharacter == '"')
            value = value.substring(1);
        final int length = value.length();
        final char lastCharacter = value.charAt(length - 1);
        if (lastCharacter == '\'' || lastCharacter == '"')
            value = value.substring(0, length - 1);
        return value;
    }

}
